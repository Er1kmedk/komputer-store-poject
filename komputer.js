class Komputer{
        constructor(name, info, price ){
                this.name=name;
                this.info=info;
                this.price=price;
        }
        getName(){
                return this.name;
        }
        getInfo(){
                return this.info;
        }
        getPrice(){
                return this.price;
        }
}