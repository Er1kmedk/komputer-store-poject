// The available laptops saved in a array
let allLaptops = []

const komputerOne = new Komputer("Intol Celloron E10", "Basic computer", 1000);
const komputerTwo = new Komputer("Intol Celloron E20", "Two thousand terraflopps", 2000);
const komputerThree = new Komputer("Fruti Qackbook", "Three amoutns of info calcylators", 3000);
const komputerFour = new Komputer("Fruti Qackbook 2", "The most expensive", 4000);

allLaptops.push(komputerOne);
allLaptops.push(komputerTwo);
allLaptops.push(komputerThree);
allLaptops.push(komputerFour);

// Bank balance
let balance = 0;

// User money
let pay = 0;

// A eventlistener change the currently shown laptop
const laptopSelector = document.getElementById("laptopSelector");
const displayedLaptop = document.getElementById("displayedLaptop");
laptopSelector.addEventListener('change', function() {
            document.getElementById("displayName").innerText = allLaptops[this.value].getName();
            document.getElementById("displayInfo").innerText = allLaptops[this.value].getInfo();
            document.getElementById("displayPrice").innerText = allLaptops[this.value].getPrice();
 
});


// This function incease the users current capital (pay) by 100kr each time it is invoked
function work() {
    const payDiv = document.getElementById("payDiv");
    pay += 100;
    payDiv.innerText = pay + " kr"
    console.log("Current funds: " + pay);
}


// This function moves the current user capital (pay) into the bank balance
function moneyTransfer() {
    const balanceDiv = document.getElementById("balanceDiv");
    balance += pay;
    pay = 0;
    payDiv.innerText = pay + " kr"
    balanceDiv.innerText = balance + " kr";
    console.log("Current balance: " + balance);
}


// This function summon the alert window for entering requested loan amount.
// It further aims to increase the bank balance but has two contraints: 
//  -Can only be used once per computer purchase.
//  -Can as a maximum double the current bank balance
function loanMoney() {
    let  isAllowedToLoan = true;
    let txt;
    var requestedAmount = prompt("Please enter requested amount:", "$$$");
    if (requestedAmount == null || requestedAmount == "") {
        txt = "User cancelled the prompt.";
    } else {
        if (isAllowedToLoan && (requestedAmount <= balance)) {
            console.log(balance);
            console.log(requestedAmount);
            balance += Number(requestedAmount);
            balanceDiv.innerText = balance + " kr";
            console.log("You were succesfully granted a loan of " + requestedAmount + " kr!");
            txt = "You were succesfully granted a loan of " + requestedAmount + " kr!"
        }
        else {
            console.log("Loan request denied!");
            txt = "Loan request denied!"
        }
    }
    document.getElementById("loanInfo").innerHTML = txt;
}

// This function is called when a customer tries to buy a laptop.
function buyNow(requestedComputer) {
    let computerPrize = 100;
    if (balance > computerPrize) {
        balance -= computerPrize
        console.log("Balance: " + balance);
        console.log("You are now the proud owner of a " + requestedComputer.getName());
    }
    else {
        console.log("Insufficient funds!");
        console.log("You can not afford this laptop!");
    }
}